import DAO.DBManager;
import DAO.UserDAOImpl;
import modules.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {
        DBManager dbManager = new DBManager();
        Connection con = dbManager.connectToDB();
        UserDAOImpl uDAO = new UserDAOImpl(con);
        List<User> users = uDAO.getAll();
        User user = uDAO.getByID(1);

        dbManager.disconnectFromDB(con);
    }
}
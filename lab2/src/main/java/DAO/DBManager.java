package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
    final private String user = "postgres";
    final private String password = "jeksonthebest";
    final private String url = "jdbc:postgresql://localhost:5432/postgres?currentSchema=aquarium";

    public Connection connectToDB() throws SQLException {

        return DriverManager.getConnection(this.url, this.user, this.password);
    }

    public void disconnectFromDB(Connection connection) throws SQLException {
        connection.close();
    }
}

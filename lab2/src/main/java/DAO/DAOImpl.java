package DAO;

import java.sql.*;
import java.util.List;


public abstract class DAOImpl <Integer, T> {
    protected Connection connection;

    public DAOImpl(Connection connection){
        this.connection = connection;
    }

    public abstract T getByID(Integer id) throws SQLException;
    public abstract List<T> getAll() throws SQLException;
}

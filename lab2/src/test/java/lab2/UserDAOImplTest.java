package lab2;

import DAO.DAOImpl;
import DAO.UserDAOImpl;
import modules.Aquarium;
import modules.Fish;
import modules.Turtule;
import modules.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Users")
@ExtendWith(MockitoExtension.class)
public class UserDAOImplTest {
    @Mock
    Connection mockConnection;

    @Mock
    Statement mockStatement;

    @Mock
    ResultSet mockResultSet;

    @BeforeEach
    void beforeEach() throws SQLException {
        when(mockConnection.createStatement()).thenReturn(mockStatement);
        when(mockStatement.executeQuery(anyString())).thenReturn(mockResultSet);
    }

    @Test
    @DisplayName("getByID")
    public void testGetUserById() throws SQLException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        UserDAOImpl UserModel = new UserDAOImpl(mockConnection);
        List<Turtule> turtleList = new ArrayList<Turtule>(){{add(new Turtule(1, "sveta", 5.4, 13, "female", true, 4.5, false));
                                                        add(new Turtule(2, "ivan", 55.4, 113, "male", true, 4.5, true));
        }};
        List<Fish> fishList = new ArrayList<Fish>(){{add(new Fish(1, "marina", 5.4, 13, "female", "yellow", true, "pointed"));
            add(new Fish(2, "stepan", 5.2, 13, "male", "black", false, "square"));
        }};
        User user = new User(1, "ivan", (short)15, new Aquarium(1, 1.5, 2.5, 3.5, turtleList, fishList));
       when(mockResultSet.getString(anyString())).thenAnswer(userOnMock -> {
            switch ((String) userOnMock.getArguments()[0]) {
                case "id":
                    return user.getId();
                case "username":
                    return user.getUsername();
                case "age":
                    return user.getAge();
                case "aquarium":
                    return user.getAquarium();
            }
            return null;
        });
        when(mockResultSet.getLong(anyString())).thenAnswer(userOnMock -> user.getId());
        assertEquals(user, UserModel.getByID(1));
    }
}
